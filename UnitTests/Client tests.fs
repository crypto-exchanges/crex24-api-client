module UnitTests

open System
open NUnit.Framework
open FsUnit

open Alex75.Cryptocurrencies
open Alex75.Crex24ApiClient


let getClient() = Client() :> IApiClient

[<Test; Category("Client")>]
let ``GetTicker`` () =
    let pair = CurrencyPair("ETH", "btc")

    let ticker = getClient().GetTicker(pair)

    ticker |> should not' (be null)
    ticker.Pair |> should equal pair


[<Test; Category("Client")>]
let ``GetTicker when pair does not exist`` () =
    let pair = CurrencyPair("xrp", "eur")  // not managed
    (fun () -> getClient().GetTicker(pair) |> ignore) |> should throw typeof<Exception>

