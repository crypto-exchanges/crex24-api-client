﻿module parser_test

open NUnit.Framework
open FsUnit
open Alex75.Cryptocurrencies


[<Test; Category("parsing")>]
let ``Parse Instruments`` () =
    let jsonString = System.IO.File.ReadAllText("data/instruments.json")
    let pairs = parser.parse_instruments jsonString
    pairs |> should contain (CurrencyPair("aaa", "btc"))

[<Test; Category("parsing")>]
let ``Parse Ticker`` () =

    let pair = CurrencyPair("eth", "eur")
    let jsonString = System.IO.File.ReadAllText("data/tickers ETH-EUR response.json")

    let ticker = parser.parse_ticker pair jsonString

    ticker |> should not' (be Null)
    ticker.Pair |> should equal pair
    ticker.Bid |> should equal 113.8
    ticker.Ask |> should equal 127.9
    ticker.High.Value |> should equal 116.9
    ticker.Low.Value |> should equal 114.3
    ticker.Last.Value |> should equal 114.32


[<Test; Category("parsing")>]
let ``Parse Ticker when pair does not exists`` () =

    let pair = CurrencyPair("xrp", "eur")
    let jsonString = System.IO.File.ReadAllText("data/tickers ETH-EUR response.json")

    let ticker = parser.parse_ticker pair jsonString

    ticker |> should not' (be Null)
    ticker.Pair |> should equal pair
    ticker.Bid |> should equal 113.8
    ticker.Ask |> should equal 127.9
    ticker.High.Value |> should equal 116.9
    ticker.Low.Value |> should equal 114.3
    ticker.Last.Value |> should equal 114.32
