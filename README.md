# CREX24 API Client 

Very simple Client for the **CREX24** exchange API.  
Target frameworks: _.NET Standard 2.0_ and _.NET Core 3.1_  

[![NuGet](https://img.shields.io/nuget/v/Alex75.Crex24ApiClient.svg)](https://www.nuget.org/packages/Alex75.Crex24ApiClient) 
![GitLab](https://gitlab.com/crypto-exchanges/crex24-api-client/badges/master/pipeline.svg)  

Repository hosted on _GitLab_.  
CD pipeline hosted on _GitLab_.  
NuGet package hosted on _NuGet_.  

CREX24 API docs: https://docs.crex24.com/trade-api/v2


## Fees




## XRP
XRP-EUR pair is not traded at the moment.  

Some pairs with XRP:
- XRP-BTC
- XRP-ETH

Some pairs with EUR:
- AYA-EUR  (Aryacoin)
- BCD-EUR  (Bitcoin Diamond)
- BSD-EUR
- __BTC-EUR__  (Bitcoin)
- BTDX-EUR
- BTX-EUR
- CEX-EUR
- DMD-EUR
- ERC-EUR
- __ETH-EUR__  (Ethereum)
- EVR-EUR
- FLASH-EUR
- FLOT-EUR
- ICR-EUR
- KNT-EUR
- KUBO-EUR
- MEC-EUR
- ONION-EUR
- POP-EUR
- PROUD-EUR
- RYO-EUR
- SCH-EUR
- SPH-EUR
- TELOS-EUR
- TYC-EUR
- USDT-EUR
- XEUR-BTC
- BTC-EURT
- DEURO-BTC


## For Developers

**Flurl** 3.x (pre) was initially used but because abandoned now, because you have the following error when used in a solution that contains other project that use the current version (2.4):  
  <div class="color: red"> Method not found: 'System.Threading.Tasks.Task`1<System.Net.Http.HttpResponseMessage> Flurl.Http.GeneratedExtensions.PostUrlEncodedAsync(Flurl.Http.IFlurlRequest, System.Object, System.Threading.CancellationToken, System.Net.Http.HttpCompletionOption)'.</div>

It uses **FSharp.Data** to parse the JSON responses.  


### GitLab CI

examples: https://gitlab.com/gitlab-org/gitlab-foss/issues/46892

nuget pack "my-project.csproj" -IncludeReferencedProjects -Build -Properties Configuration=Release -Version 1.0.0
nuget push \*.nupkg %NugetAPIKey%

- 'nuget pack "my-project/my-project.csproj" -IncludeReferencedProjects -Build -Properties Configuration=Release -Version 0.0.1.%CI_BUILD_ID%'
- 'nuget push \*.nupkg %NugetAPIKey% -Source %NugetSource%'

