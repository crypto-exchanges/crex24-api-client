[<NUnit.Framework.Category("Client")>]
module IntegrationTests

open NUnit.Framework
open FsUnit
open Alex75.Crex24ApiClient
open Alex75.Cryptocurrencies

let getClient() = Client() :> IClient

[<Test>]
let ``ListPairs`` () =
    let pairs = getClient().ListPairs()
    pairs |> should not' (be Null)


[<Test>] 
let ``GetTicker`` () =
    let pair = CurrencyPair("xrp", "btc")
    let ticker = getClient().GetTicker(pair)
    ticker |> should not' (be Null)
    ticker.Pair |> should equal pair

