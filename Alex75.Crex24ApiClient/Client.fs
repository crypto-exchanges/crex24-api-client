﻿namespace Alex75.Crex24ApiClient

open System
open System.Collections.Generic
open Flurl.Http
open Alex75.Cryptocurrencies


type public Client() =     

    let base_url = "https://api.crex24.com/v2"
    let cache = Cache()
    let assets_cache_time = TimeSpan.FromHours 6.0
    let ticker_cache_time = TimeSpan.FromSeconds 10.0
    let balance_cache_time = TimeSpan.FromSeconds 30.0    

    interface IClient 

    interface IApiClient with

        member this.ListPairs(): ICollection<CurrencyPair> = 
            match cache.GetPairs assets_cache_time with
            | Some pairs -> pairs
            | _ -> 
                let pairs = parser.parse_instruments ((sprintf"%s/public/instruments" base_url).GetStringAsync().Result)
                cache.SetPairs pairs
                pairs :> ICollection<CurrencyPair>   

        member this.GetTicker(pair: CurrencyPair): Ticker = 
            let url = sprintf "%s/public/tickers?instrument=%s" base_url pair.Dashed
            try
                match cache.GetTicker pair ticker_cache_time with 
                | Some ticker -> ticker
                | _ ->
                    let response = url.AllowHttpStatus("400").GetAsync().Result
                    let contentString = response.Content.ReadAsStringAsync().Result
                    if not(response.IsSuccessStatusCode) then failwith (parser.parse_error contentString)
                    else 
                        let ticker = parser.parse_ticker pair contentString
                        cache.SetTicker ticker
                        ticker
            with exc -> 
                failwithf "API call failed for %s. %s" (pair.Dashed) exc.Message

        
