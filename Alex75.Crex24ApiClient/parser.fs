﻿module parser

open System.Collections.Generic
open FSharp.Data
open Alex75.Cryptocurrencies


let parse_error jsonString = 
   JsonValue.Parse(jsonString).["errorDescription"].AsString()  //{"errorDescription":"Parameter 'instrument' contains invalid value."}

let parse_instruments jsonString = 

    let items = JsonValue.Parse(jsonString).AsArray()
    let pairs = List<CurrencyPair>()
    for item in items do
        pairs.Add (CurrencyPair(item.["baseCurrency"].AsString(), item.["quoteCurrency"].AsString()))

    pairs

let parse_ticker pair jsonString =
    let json = JsonValue.Parse(jsonString).AsArray()
    if (json.Length = 0) then failwith "No pair found" 
    let item = json.[0]

    let bid = item.["bid"].AsDecimal()
    let ask = item.["ask"].AsDecimal()

    let low = item.["low"].AsDecimal()
    let high = item.["high"].AsDecimal()
    let last = item.["last"].AsDecimal()

    Ticker(pair, bid, ask, Some(low), Some(high), Some(last))